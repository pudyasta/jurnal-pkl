<?php

namespace App\Http\Controllers;

use App\Jurnal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class JurnalController extends Controller
{
    static function upFoto(Request $request)
    {
        $file1 = $request->file('foto1');
        $file2 = $request->file('foto2');
        $file3 = $request->file('foto3');
        if ($file1 != NULL) {
            $name1 = uniqid() . '.' . $file1->getClientOriginalExtension();
            $file1->storeAs('public', $name1);
        } else {
            $name1 = NULL;
        }
        if ($file2 != NULL) {
            $name2 = uniqid() . '.' . $file2->getClientOriginalExtension();
            $file2->storeAs('public', $name2);
        } else {
            $name2 = NULL;
        }
        if ($file3 != NULL) {
            $name3 = uniqid() . '.' . $file3->getClientOriginalExtension();
            $file3->storeAs('public', $name3);
        } else {
            $name3 = NULL;
        }
        return [$name1, $name2, $name3];
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jurnal = DB::table('jurnals')->get();
        return view('index', ['jurnals' => $jurnal]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $raw = JurnalController::upFoto($request);

        $insert = DB::table('jurnals')->insert([
            'hari_ke' => $request['hari'],
            'tanggal' => $request['tanggal'],
            'kegiatan' => $request['kegiatan'],
            'deskripsi' => $request['deskripsi'],
            'foto1' => $raw[0],
            'foto2' => $raw[1],
            'foto3' => $raw[2]
        ]);
        if ($insert) {
            return redirect('/home');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Jurnal  $jurnal
     * @return \Illuminate\Http\Response
     */
    public function show(Jurnal $jurnal, $id)
    {
        $jurnals = DB::table('jurnals')->where('id', $id)->first();
        return view('view', ['jurnals' => $jurnals]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Jurnal  $jurnal
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $jurnals = DB::table('jurnals')->where('id', $id)->first();
        return view('edit', ['jurnals' => $jurnals]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Jurnal  $jurnal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Jurnal $jurnal, $id)
    {
        $raw = JurnalController::upFoto($request);

        if ($request->foto1 == NULL) {
            $name1 = $request->hidden1;
        } else {
            $name1 = $raw[0];
        }


        if ($request->foto2 == NULL) {
            $name2 = $request->hidden2;
        } else {
            $name2 = $raw[1];
        }


        if ($request->foto3 == NULL) {
            $name3 = $request->hidden3;
        } else {
            $name3 = $raw[2];
        }
        $affected = DB::table('jurnals')
            ->where('id', $id)
            ->update([
                'hari_ke' => $request['hari'],
                'tanggal' => $request['tanggal'],
                'kegiatan' => $request['kegiatan'],
                'deskripsi' => $request['deskripsi'],
                'foto1' => $name1,
                'foto2' => $name2,
                'foto3' => $name3
            ]);
        if ($affected) {
            return redirect('/home');
        } else {
            return 'goblok';
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Jurnal  $jurnal
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        Jurnal::destroy($id);
        return redirect('/home');
    }
}
