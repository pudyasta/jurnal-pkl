<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jurnal extends Model
{
    protected $fillable = [
        'hari_ke', 'tanggal', 'kegiatan', 'deskripsi', 'foto1', 'foto2', 'foto3'
    ];
}
