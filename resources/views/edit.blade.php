@extends("main")
@section('sidebar')
<div class="container mt-4">
    <form action="/edit/{{$jurnals->id}}" method="POST" enctype="multipart/form-data">
        @method('PUT')
        @csrf

        <label for="hari">Hari Ke</label>
        <input type="number" class="form-control" name="hari" id="hari" value="{{$jurnals->hari_ke}}">
        <label for="tanggal">Tanggal</label>
        <input type="date" class="form-control" name="tanggal" id="tanggal" value="{{$jurnals->tanggal}}">
        <label for="kegiatan">Kegiatan</label>
        <input type="text" class="form-control" name="kegiatan" id="kegiatan" value="{{$jurnals->kegiatan}}">
        <label for=" deskripsi">Deskripsi</label>
        <textarea type="text" class="form-control" name="deskripsi" id="deskripsi">{{$jurnals->deskripsi}}</textarea>

        <label for="foto1">Foto1</label>
        <input type="file" class="form-control" name="foto1" id="foto1">
        <input type="hidden" value="{{$jurnals->foto1}}" name="hidden1">
        <input type="hidden" value="{{$jurnals->foto2}}" name="hidden2">
        <input type="hidden" value="{{$jurnals->foto3}}" name="hidden3">
        <label for="foto2">foto2</label>
        <input type="file" class="form-control" name="foto2" id="foto2">
        <label for="foto3">foto3</label>
        <input type="file" class="form-control" name="foto3" id="foto3">


        <a href="/home" class="btn btn-secondary mt-3">Kembali</a>

        <button type="submit" class="btn btn-primary mt-3">Kirim</button>
    </form>
</div>
@endsection