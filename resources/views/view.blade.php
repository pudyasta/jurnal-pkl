@extends("main")
@section('sidebar')
<div class="container mt-4">
    <h1>Hari ke {{$jurnals->hari_ke}}</h1>
    <h4>Tanggal {{$jurnals->tanggal}}</h4>

    <div class="row ">
        <div class="col d-flex justify-content-center mt-3"><img src="{{asset('storage/'.$jurnals->foto1)}}" alt="" width="350px"></div>
        <div class="col d-flex justify-content-center mt-3"><img src="{{asset('storage/'.$jurnals->foto2)}}" alt="" width="350px"></div>
        <div class="col d-flex justify-content-center mt-3"><img src="{{asset('storage/'.$jurnals->foto3)}}" alt="" width="350px"></div>
    </div>
    <h3 class="mt-5">{{$jurnals->kegiatan}}</h3>
    <h5>{{$jurnals->deskripsi}}</h5>
    <a href="/home" class="btn btn-secondary">Kembali</a>
</div>
@endsection