@extends('main')

@section('sidebar')
<!-- Button trigger modal -->

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body modal-lg">
                <form action="/create" method="POST" enctype="multipart/form-data">
                    @csrf
                    <label for="hari">Hari Ke</label>
                    <input type="number" class="form-control" name="hari" id="hari">
                    <label for="tanggal">Tanggal</label>
                    <input type="date" class="form-control" name="tanggal" id="tanggal">
                    <label for="kegiatan">Kegiatan</label>
                    <input type="text" class="form-control" name="kegiatan" id="kegiatan">
                    <label for="deskripsi">Deskripsi</label>
                    <textarea type="text" class="form-control" name="deskripsi" id="deskripsi"></textarea>
                    <?php for ($i = 1; $i < 4; $i++) : ?>
                        <label for="foto{{$i}}">Foto{{$i}}</label>
                        <input type="file" class="form-control" name="foto{{$i}}" id="foto{{$i}}">
                    <?php endfor ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Kirim</button>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="container mt-5">
    <h1>Jurnal PKL</h1>
    <button type="button" class="btn btn-primary mt-3" data-toggle="modal" data-target="#exampleModal">
        Tambah
    </button>

    <table class="table mt-3">
        <thead class="thead-dark">
            <tr>
                <th scope="col">Act</th>
                <th scope="col">Hari-Ke</th>
                <th scope="col">Tanggal</th>
                <th scope="col">Kegiatan</th>
                <th scope="col">Deskripsi</th>
                <th scope="col">Foto</th>
                <th scope="col">Foto</th>
                <th scope="col">Foto</th>
            </tr>
        </thead>
        <tbody>
            @foreach($jurnals as $jurnal)
            <tr>
                <th scope="row">
                    <a href="/delete/{{$jurnal->id}}"><span class="badge badge-danger mx-1">Delete</span></a>
                    <a href="/edit/{{$jurnal->id}}"><span class="badge badge-warning mx-1">Edit</span></a>
                    <a href="/view/{{$jurnal->id}}"><span class="badge badge-primary mx-1">View</span></a></th>
                <td>{{$jurnal->hari_ke}}</td>
                <td>{{$jurnal->tanggal}}</td>
                <td>{{$jurnal->kegiatan}}</td>
                <td>{{$jurnal->deskripsi}}</td>
                <td><img src="{{asset('storage/'.$jurnal->foto1)}}" alt="" width="100px"></td>
                <td><img src="{{asset('storage/'.$jurnal->foto2)}}" alt="" width="100px"></td>
                <td><img src="{{asset('storage/'.$jurnal->foto3)}}" alt="" width="100px"></td>
            </tr>
            @endforeach

        </tbody>
    </table>
</div>
@endsection