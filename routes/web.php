<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', 'JurnalController@index');
Route::get('/delete/{id}', 'JurnalController@destroy');
Route::get('/edit/{id}', 'JurnalController@edit');
Route::get('/view/{id}', 'JurnalController@show');

Route::put('/edit/{id}', 'JurnalController@update');



Route::post('/create', 'JurnalController@store');

Route::get('/', function () {
    return view('welcome');
});
